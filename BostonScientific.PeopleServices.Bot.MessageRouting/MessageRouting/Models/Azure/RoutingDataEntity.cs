﻿using Microsoft.WindowsAzure.Storage.Table;

namespace BostonScientific.PeopleServices.Bot.MessageRouting.Models.Azure
{
    public class RoutingDataEntity : TableEntity
    {
        public string Body { get; set; }
    }
}