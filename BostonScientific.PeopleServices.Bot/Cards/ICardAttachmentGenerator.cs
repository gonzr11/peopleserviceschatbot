﻿using Microsoft.Bot.Schema;

namespace BostonScientific.PeopleServices.Bot.Cards
{
    public interface ICardAttachmentGenerator
    {
        Attachment CreateGreetingsCardAttachment(string channelId);
        Attachment CreateWelcomeCardAttachment(string channelId);
    }
}