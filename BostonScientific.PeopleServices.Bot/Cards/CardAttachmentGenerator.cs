﻿using Microsoft.Bot.Schema;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace BostonScientific.PeopleServices.Bot.Cards
{
    public class CardAttachmentGenerator : ICardAttachmentGenerator
    {
        private List<string> customAdaptiveCardsChannels;
        private string defaultAdaptiveCardPrefix = "default";
        IConfiguration _configuration;
        public CardAttachmentGenerator(IConfiguration configuration)
        {
            _configuration = configuration;
            var hasCustomCardForChannels = !string.IsNullOrEmpty(configuration["CustomAdaptiveCardsChannels"]);
            if (hasCustomCardForChannels)
            {
                customAdaptiveCardsChannels = configuration["CustomAdaptiveCardsChannels"].Split(',').ToList();
            }
            else
            {
                customAdaptiveCardsChannels = new List<string>();
            }
        }
        public Attachment CreateWelcomeCardAttachment(string channelId)
        {
            string adaptiveCardPrefix = customAdaptiveCardsChannels.Contains(channelId) ? channelId : defaultAdaptiveCardPrefix;
            var cardResourcePath = $"BostonScientific.PeopleServices.Bot.Cards.{adaptiveCardPrefix.ToLower()}WelcomeCard.json";

            using (var stream = GetType().Assembly.GetManifestResourceStream(cardResourcePath))
            {
                using (var reader = new StreamReader(stream))
                {
                    var adaptiveCard = reader.ReadToEnd();
                    return new Attachment()
                    {
                        ContentType = "application/vnd.microsoft.card.adaptive",
                        Content = JsonConvert.DeserializeObject(adaptiveCard),
                    };
                }
            }
        }

        public Attachment CreateGreetingsCardAttachment(string channelId)
        {
            string adaptiveCardPrefix = customAdaptiveCardsChannels.Contains(channelId) ? channelId : defaultAdaptiveCardPrefix;
            var cardResourcePath = $"BostonScientific.PeopleServices.Bot.Cards.{adaptiveCardPrefix.ToLower()}GreetingsCard.json";

            using (var stream = GetType().Assembly.GetManifestResourceStream(cardResourcePath))
            {
                using (var reader = new StreamReader(stream))
                {
                    var adaptiveCard = reader.ReadToEnd();
                    return new Attachment()
                    {
                        ContentType = "application/vnd.microsoft.card.adaptive",
                        Content = JsonConvert.DeserializeObject(adaptiveCard),
                    };
                }
            }
        }
    }
}
