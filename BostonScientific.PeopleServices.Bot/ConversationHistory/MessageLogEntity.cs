﻿using Microsoft.WindowsAzure.Storage.Table;
using System;

namespace BostonScientific.PeopleServices.Bot.ConversationHistory
{
    public class MessageLogEntity : TableEntity
    {
        public string Body
        {
            get;
            set;
        }
    }
}
