﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

using System.Linq;

namespace BostonScientific.PeopleServices.Bot.CognitiveModels
{
    // Extends the partial PeopleServicesRequest class with methods and properties that simplify accessing entities in the luis results
    public partial class PeopleServicesRequest
    {
        public (string Command, string AgentCommand) AgentCommandEntities
        {
            get
            {
                var commandValue = Entities?._instance?.Command?.FirstOrDefault()?.Text;
                var agentCommandValue = Entities?.Command?.FirstOrDefault()?.AgentCommand?.FirstOrDefault()?.FirstOrDefault();
                return (commandValue, agentCommandValue);
            }
        }        

        // This value will be a TIMEX. And we are only interested in a Date so grab the first result and drop the Time part.
        // TIMEX is a format that represents DateTime expressions that include some ambiguity. e.g. missing a Year.
        public string EmployeeId
            => Entities.EmployeeId?.FirstOrDefault();
    }
}
