// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.
//
// Generated with Bot Builder V4 SDK Template for Visual Studio CoreBot v4.5.0

namespace BostonScientific.PeopleServices.Bot
{
    public class RequestDetails
    {
        public string EmployeeId { get; set; }
    }
}
