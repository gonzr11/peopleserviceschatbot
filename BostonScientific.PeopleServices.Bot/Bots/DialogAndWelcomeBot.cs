// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.
//
// Generated with Bot Builder V4 SDK Template for Visual Studio CoreBot v4.5.0

using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using BostonScientific.PeopleServices.Bot.Cards;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Schema;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace BostonScientific.PeopleServices.Bot.Bots
{
    public class DialogAndWelcomeBot<T> : DialogBot<T>
        where T : Dialog
    {
        ICardAttachmentGenerator _cardAttachmentGenerator;
        public DialogAndWelcomeBot(ConversationState conversationState, UserState userState, ICardAttachmentGenerator cardAttachmentGenerator, T dialog)
            : base(conversationState, userState, dialog)
        {
            _cardAttachmentGenerator = cardAttachmentGenerator;
        }

        protected override async Task OnMembersAddedAsync(IList<ChannelAccount> membersAdded, ITurnContext<IConversationUpdateActivity> turnContext, CancellationToken cancellationToken)
        {
            foreach (var member in membersAdded)
            {
                // Greet anyone that was not the target (recipient) of this message.
                // To learn more about Adaptive Cards, see https://aka.ms/msbot-adaptivecards for more details.
                if (member.Id != turnContext.Activity.Recipient.Id)
                {
                    var welcomeCard = _cardAttachmentGenerator.CreateWelcomeCardAttachment(turnContext.Activity.ChannelId);
                    var response = MessageFactory.Attachment(welcomeCard);
                    await turnContext.SendActivityAsync(response, cancellationToken);
                    await Dialog.RunAsync(turnContext, ConversationState.CreateProperty<DialogState>("DialogState"), cancellationToken);
                }
            }
        }       
    }
}

