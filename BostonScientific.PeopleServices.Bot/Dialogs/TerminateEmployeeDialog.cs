// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.
//
// Generated with Bot Builder V4 SDK Template for Visual Studio CoreBot v4.5.0

using System.Threading;
using System.Threading.Tasks;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Schema;
using Microsoft.Recognizers.Text.DataTypes.TimexExpression;

namespace BostonScientific.PeopleServices.Bot.Dialogs
{
    public class TerminateEmployeeDialog : CancelAndHelpDialog
    {
        private const string EmployeeIdStepMsgText = "What is the Employee ID?";
        public TerminateEmployeeDialog()
            : base(nameof(TerminateEmployeeDialog))
        {
            AddDialog(new ConfirmPrompt(nameof(ConfirmPrompt)));
            AddDialog(new WaterfallDialog(nameof(WaterfallDialog), new WaterfallStep[]
            {
                EmployeeIdStepAsync,
                ConfirmStepAsync,
                FinalStepAsync,
            }));

            // The initial child Dialog to run.
            InitialDialogId = nameof(WaterfallDialog);
        }
        private async Task<DialogTurnResult> EmployeeIdStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            var employeeDetails = (RequestDetails)stepContext.Options;
    
            if (employeeDetails.EmployeeId == null)
            {
                var promptMessage = MessageFactory.Text(EmployeeIdStepMsgText, EmployeeIdStepMsgText, InputHints.ExpectingInput);
                return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = promptMessage }, cancellationToken);
            }

            return await stepContext.NextAsync(employeeDetails.EmployeeId, cancellationToken);
        }

        private async Task<DialogTurnResult> ConfirmStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            var employeeDetails = (RequestDetails)stepContext.Options;
            employeeDetails.EmployeeId = (string)stepContext.Result;
            var messageText = $"Please confirm, I have you want to terminate employee: {employeeDetails.EmployeeId}. Is this correct?";
            var promptMessage = MessageFactory.Text(messageText, messageText, InputHints.ExpectingInput);

            return await stepContext.PromptAsync(nameof(ConfirmPrompt), new PromptOptions { Prompt = promptMessage }, cancellationToken);
        }

        private async Task<DialogTurnResult> FinalStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            if ((bool)stepContext.Result)
            {
                var employeeDetails = (RequestDetails)stepContext.Options;

                return await stepContext.EndDialogAsync(employeeDetails, cancellationToken);
            }

            return await stepContext.EndDialogAsync(null, cancellationToken);
        }
    }
}
