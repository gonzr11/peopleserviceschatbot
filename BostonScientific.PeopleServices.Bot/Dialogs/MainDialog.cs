// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.
//
// Generated with Bot Builder V4 SDK Template for Visual Studio CoreBot v4.5.0

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Schema;
using Microsoft.Extensions.Logging;
using Microsoft.Recognizers.Text.DataTypes.TimexExpression;

using BostonScientific.PeopleServices.Bot.CognitiveModels;
using System.IO;
using Newtonsoft.Json;
using BostonScientific.PeopleServices.Bot.Cards;

namespace BostonScientific.PeopleServices.Bot.Dialogs
{
    public class MainDialog : ComponentDialog
    {
        private readonly PeopleServiceRecognizer _luisRecognizer;
        ICardAttachmentGenerator _cardAttachmentGenerator;
        protected readonly ILogger Logger;

        // Dependency injection uses this constructor to instantiate MainDialog
        public MainDialog(PeopleServiceRecognizer luisRecognizer, TerminateEmployeeDialog requestDialog, ICardAttachmentGenerator cardAttachmentGenerator, ILogger<MainDialog> logger)
            : base(nameof(MainDialog))
        {
            _luisRecognizer = luisRecognizer;
            Logger = logger;
            _cardAttachmentGenerator = cardAttachmentGenerator;
            AddDialog(new TextPrompt(nameof(TextPrompt)));
            AddDialog(requestDialog);
            AddDialog(new WaterfallDialog(nameof(WaterfallDialog), new WaterfallStep[]
            {
                IntroStepAsync,
                ActStepAsync,
                FinalStepAsync,
            }));

            // The initial child Dialog to run.
            InitialDialogId = nameof(WaterfallDialog);
        }
        // Load attachment from embedded resource.
    
        private async Task<DialogTurnResult> IntroStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            if (!_luisRecognizer.IsConfigured)
            {
                await stepContext.Context.SendActivityAsync(
                    MessageFactory.Text("NOTE: LUIS is not configured. To enable all capabilities, add 'LuisAppId', 'LuisAPIKey' and 'LuisAPIHostName' to the appsettings.json file.", inputHint: InputHints.IgnoringInput), cancellationToken);

                return await stepContext.NextAsync(null, cancellationToken);
            }

            // Use the text provided in FinalStepAsync or the default if it is the first time.
            var messageText = stepContext.Options?.ToString() ?? "What can I help you with today?\n";
            var promptMessage = MessageFactory.Text(messageText, messageText, InputHints.ExpectingInput);
            return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = promptMessage }, cancellationToken);
        }

        private async Task<DialogTurnResult> ActStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            if (!_luisRecognizer.IsConfigured)
            {
                // LUIS is not configured, we just run the RequestDialog path with an empty RequestDetailsInstance.
                //return await stepContext.BeginDialogAsync(nameof(RequestDialog), new RequestDetails(), cancellationToken);
                return await stepContext.CancelAllDialogsAsync();
            }

            // Call LUIS and gather any potential request details. (Note the TurnContext has the response to the prompt.)
            var luisResult = await _luisRecognizer.RecognizeAsync<PeopleServicesRequest>(stepContext.Context, cancellationToken);
            switch (luisResult.TopIntent().intent)
            {
                case PeopleServicesRequest.Intent.Greetings:
                    // Initialize Greetings with any entities we may have found in the response.
                    var welcomeCard = _cardAttachmentGenerator.CreateGreetingsCardAttachment(stepContext.Context.Activity.ChannelId);
                    var response = MessageFactory.Attachment(welcomeCard);                
                    await stepContext.Context.SendActivityAsync(response, cancellationToken);
                    return await stepContext.ReplaceDialogAsync(InitialDialogId, null, cancellationToken);
                case PeopleServicesRequest.Intent.TerminateEmployee:
                    // Initialize TerminateEmployee with any entities we may have found in the response.
                    var requestDetails = new RequestDetails()
                    {
                        // Get destination and origin from the composite entities arrays.
                        EmployeeId = luisResult.EmployeeId
                    };
                    // Run the RequestDialog giving it whatever details we have from the LUIS call, it will fill out the remainder.
                    return await stepContext.BeginDialogAsync(nameof(TerminateEmployeeDialog), requestDetails, cancellationToken);                   
                default:
                    // Catch all for unhandled intents
                    var didntUnderstandMessageText = $"Sorry, I didn't get that. Please try asking in a different way (intent was {luisResult.TopIntent().intent})";
                    var didntUnderstandMessage = MessageFactory.Text(didntUnderstandMessageText, didntUnderstandMessageText, InputHints.IgnoringInput);
                    await stepContext.Context.SendActivityAsync(didntUnderstandMessage, cancellationToken);
                    break;
            }

            return await stepContext.NextAsync(null, cancellationToken);
        }

   

        private async Task<DialogTurnResult> FinalStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            // the Result here will be null.
            if (stepContext.Result is RequestDetails result)
            {
                // Now we have all the request details call the request service.

                // If the call to the request service was successful tell the user.
                var messageText = $"Thank you for contacting us";
                var message = MessageFactory.Text(messageText, messageText, InputHints.IgnoringInput);
                await stepContext.Context.SendActivityAsync(message, cancellationToken);
            }

            // Restart the main dialog with a different message the second time around
            var promptMessage = "What else can I do for you?";
            return await stepContext.ReplaceDialogAsync(InitialDialogId, promptMessage, cancellationToken);
        }
    }
}
